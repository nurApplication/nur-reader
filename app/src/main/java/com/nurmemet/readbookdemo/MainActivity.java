package com.nurmemet.readbookdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;

import com.nurmemet.readbook.buidler.ReaderBuilder;
import com.nurmemet.readbook.utils.FileUtils;
import com.nurmemet.readbook.utils.NurReaderController;
import com.nurmemet.readbook.widget.NurReaderView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NurReaderView nurReaderView = findViewById(R.id.nurReaderView);

        //reader builder
        ReaderBuilder readerBuilder = new ReaderBuilder(this) {
            @Override
            public String getData() {
                return FileUtils.readTxt(FileUtils.PATH + "test.txt");//读取本地TXT文件
//                return getString(R.string.test);
            }

            @Override
            public String getTitle() {
                return "Title";
            }

        };
        readerBuilder.setController(new NurReaderController(this, new NurReaderController.OnClickIconListener() {
            /**
             * 点击返回按钮
             */
            @Override
            public void onBackPress(View v) {
                finish();
            }

            /**
             * 点击收藏按钮
             */
            @Override
            public void onCollectionPress(View v) {
            }
        }));
        readerBuilder.setStackFromEnd(true);
        nurReaderView.init(readerBuilder);


    }

}


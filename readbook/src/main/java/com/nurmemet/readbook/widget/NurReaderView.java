package com.nurmemet.readbook.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.nurmemet.readbook.R;
import com.nurmemet.readbook.buidler.NurReaderThemeData;
import com.nurmemet.readbook.buidler.ReaderBuilder;
import com.nurmemet.readbook.adapter.ReaderTxtAdapter;
import com.nurmemet.readbook.myInreface.OnChangeListener;
import com.nurmemet.readbook.utils.OnTouchReader;
import com.nurmemet.readbook.utils.ReaderUtils;
import com.nurmemet.readbook.utils.ResourcesUtils;

import java.text.DecimalFormat;
import java.util.List;

import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_DRAGGING;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE;

/**
 * Date:2020-11-07
 * Author:Nurmemet
 * Email:nur01@qq.com
 * Blog:https://www.cnblogs.com/nurmemet/
 */
public class NurReaderView extends RelativeLayout implements ReaderUtils.OnResult, OnTouchReader.RenderTouchListener {

    private static final String TAG = "NurReaderView";

    private Context mContext;
    private RecyclerView mRecyclerView;
    private TextView mTitleView;
    private TextView mTextItem;
    private TextView mPercentTv;
    private ReaderBuilder mBuilder;
    private ReaderTxtAdapter mAdapter;
    private int mSelectPosition = 0;
    private boolean mStackFromEnd;
    private boolean firstLoad = true;
    private OnChangeListener mBuilderChangeListener;
    private OnChangeListener mPageChangeListener;
    public ViewGroup mRootView;
    private NurController mController;
    private NurReaderThemeData mTextAttribute;
    private View mProgressBar;
    private ReaderUtils mReaderUtilsNew;


    public NurReaderView(Context context) {
        this(context, null);
    }

    public NurReaderView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NurReaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        LayoutInflater.from(mContext).inflate(R.layout.nur_reader_view, this);
        initView();
    }

    /**
     * 初始化views
     */
    private void initView() {
        setBackgroundColor(ResourcesUtils.getColor(mContext, R.color.book_bg));
        mRootView = findViewById(R.id.nur_reader_root_view);
        mRecyclerView = findViewById(R.id.nur_reader_recycler_view);
        mTitleView = findViewById(R.id.nur_reader_title_view);
        mTextItem = findViewById(R.id.nur_reader_item_text_view);
        mPercentTv = findViewById(R.id.nur_reader_percent_tv);
        mProgressBar = findViewById(R.id.nur_reader_progress_bar);

        mAdapter = new ReaderTxtAdapter();
        mRecyclerView.setAdapter(mAdapter);
        ///获取当前位置
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == SCROLL_STATE_IDLE || newState == SCROLL_STATE_DRAGGING) {
                    // DES: 找出当前可视Item位置
                    RecyclerView.LayoutManager layoutManager = mRecyclerView.getLayoutManager();
                    if (layoutManager instanceof LinearLayoutManager) {
                        LinearLayoutManager linearManager = (LinearLayoutManager) layoutManager;
                        mSelectPosition = linearManager.findFirstVisibleItemPosition();
                        getPercent();
                    }
                }
            }
        });
    }


    /**
     * 开始干活(ReaderBuilder是null的话不会调用)
     */
    private void loadData() {
        if (mBuilder == null) {
            Log.e(TAG, "builder is null");
            return;
        }
        mProgressBar.setVisibility(VISIBLE);
        mStackFromEnd = mBuilder.isStackFromEnd();
        mBuilderChangeListener = mBuilder.getChangeListener();
        mTitleView.setText(mBuilder.getTitle());
        mRecyclerView.setLayoutManager(mBuilder.getLayoutManager());
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(mRecyclerView);
        mAdapter.setTouchListener(this);
//        ReaderTextUtils.sortTxt(mTextItem, mBuilder.getData(), this);
        mReaderUtilsNew = new ReaderUtils();
        mReaderUtilsNew.start(mTextItem, mBuilder.getData(), this);

        //init controller
        mController = mBuilder.getNurController();
        if (mController != null) {
            mPageChangeListener = mController.init(this, mBuilder);
        }
    }


    /**
     * 阅读百分之几
     */
    private void getPercent() {
        int maxPage = mAdapter.getItemCount();
        if (maxPage <= 0) {
            return;
        }

        if (mSelectPosition > maxPage - 1) {
            mSelectPosition = 0;
        }

        int i = mSelectPosition + 1;
        int percent = i * 100 / maxPage;
        DecimalFormat format = new DecimalFormat("#.0");
        String percentSrt = i + "/" + maxPage + "   " + format.format(percent) + "%";
        mPercentTv.setText(percentSrt);
        if (mBuilderChangeListener != null) {
            mBuilderChangeListener.onChange(mSelectPosition, mAdapter.getItemCount(), percent);
        }
        if (mPageChangeListener != null) {
            mPageChangeListener.onChange(mSelectPosition, mAdapter.getItemCount(), percent);
        }
    }


    /**
     * 初始化阅读器
     *
     * @param builder ReaderBuilder 可以自定义阅读器（controller|阅读方向等）
     */
    public void init(ReaderBuilder builder) {
        this.mBuilder = builder;
        loadData();
    }


    /**
     * 自动翻页
     *
     * @param index 翻到《index》
     */
    public void setCurrentIndex(int index) {
        List<String> data = mAdapter.getData();
        if (index < 0 || data == null)
            return;
        if (data.size() <= index) {
            index = data.size() - 1;
        }
        if (Math.abs(mSelectPosition - index) > 3) {
            mRecyclerView.scrollToPosition(index);
        } else {
            mRecyclerView.smoothScrollToPosition(index);
        }
        mSelectPosition = index;
    }


    /**
     * 自动翻页
     * 根据百分之
     *
     * @param percent 翻到《index》
     */
    public void setCurrentPercent(int percent, int maxPercent) {
        List<String> data = mAdapter.getData();
        if (data == null)
            return;
        int size = data.size();
        if (size <= 0)
            return;

        float index = percent / ((float) maxPercent / size);
        setCurrentIndex((int) index);
    }

    /**
     * 主题
     */
    protected void setTheme(NurReaderThemeData textAttribute) {
        mTextAttribute = textAttribute;
        if (mTextAttribute == null) return;

        float textSize = mTextAttribute.getTextSize();
        if (textSize != 0) {
            mTextItem.setTextSize(textSize);
        }

        int textColor = mTextAttribute.getTextColor();
        if (textColor != 0) {
            mTextItem.setTextColor(textColor);
        }

        mReaderUtilsNew.start(mTextItem, mBuilder.getData(), this);
    }


    /**
     * @return select index
     */
    public int getSelectPosition() {
        return mSelectPosition;
    }

    @Override
    public void result(List<String> sortData) {
        if (mAdapter != null) {
            if (mProgressBar.getVisibility() != GONE)
                mProgressBar.setVisibility(GONE);
            mAdapter.setData(sortData, mTextAttribute);
            getPercent();
        }
        if (mRecyclerView != null && mStackFromEnd && firstLoad) {
            mRecyclerView.scrollToPosition(0);
        }
        firstLoad = false;
    }


    @Override
    public void onClickLeft(View view) {
        if (mController != null && mController.isShowControl) {
            mController.showAndDismissControl();
            return;
        }
        setCurrentIndex(mStackFromEnd ? mSelectPosition + 1 : mSelectPosition - 1);
    }

    @Override
    public void onClickCenter(View view) {
        if (mController != null) {
            mController.showAndDismissControl();
        }
    }

    @Override
    public void onClickRight(View view) {
        if (mController != null && mController.isShowControl) {
            mController.showAndDismissControl();
            return;
        }
        setCurrentIndex(mStackFromEnd ? mSelectPosition - 1 : mSelectPosition + 1);
    }
}

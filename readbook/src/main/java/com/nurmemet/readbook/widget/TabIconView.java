package com.nurmemet.readbook.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.nurmemet.readbook.R;
import com.nurmemet.readbook.utils.DpUtils;


/**
 * Date:2020-11-02
 * Author:nurmemet
 * Email:nur01@qq.com
 * Blog:https://www.cnblogs.com/nurmemet/
 */
public class TabIconView extends LinearLayout {
    private final Context mContext;

    //icon size
    private int mIconSize;
    private int mIcon;
    private int mPaddingTextTop;
    private int mTextSize = 12;
    private ImageView mImageView;
    private String mName;
    private ColorStateList mTextColor;
    private TextView mTextView;


    public TabIconView(Context context) {
        this(context, null);
    }

    public TabIconView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public TabIconView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        //默认值
        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER);
        mIconSize = DpUtils.dip2px(context, 22);
        mContext = context;

        //初始化
        if (attrs != null) {
            initAttrs(attrs);
        }
        initView();
    }

    /**
     * 初始化attrs (获取属性)
     */
    private void initAttrs(AttributeSet attrs) {
        TypedArray a = mContext.obtainStyledAttributes(attrs, R.styleable.TabIconView);
        mIconSize = (int) a.getDimension(R.styleable.TabIconView_iconSize, mIconSize);
        mName = a.getString(R.styleable.TabIconView_text);
        mIcon = a.getResourceId(R.styleable.TabIconView_icon, 0);
        mTextColor = a.getColorStateList(R.styleable.TabIconView_textColor);
        mPaddingTextTop = (int) a.getDimension(R.styleable.TabIconView_paddingTextTop, 0);
        a.recycle();
    }


    /**
     * 初始化view
     */
    private void initView() {
        mImageView = new ImageView(mContext);
        mImageView.setImageResource(mIcon);
        setImagePrams();
        addView(mImageView);


        LayoutInflater.from(mContext).inflate(R.layout.nur_reader_text_view_layout, this);
        mTextView = findViewById(R.id.textView);
        mTextView.setText(mName);
        mTextView.setTextSize(mTextSize);
        if (mTextColor != null)
            mTextView.setTextColor(mTextColor);
        mTextView.setPadding(0, mPaddingTextTop, 0, 0);
    }

    /**
     * mImageView.setLayoutParams()
     */
    private void setImagePrams() {
        LayoutParams param = new LayoutParams(mIconSize, mIconSize);
        mImageView.setLayoutParams(param);
    }


    /**
     * setIconSize
     *
     * @param iconSize 图标大小
     */
    public void setIconSize(int iconSize) {
        this.mIconSize = iconSize;
        setImagePrams();
    }

    public void setTextSize(int mTextSize) {
        this.mTextSize = mTextSize;
        mTextView.setTextSize(mTextSize);
    }

    public void setTextColor(int color) {
        mTextColor = ColorStateList.valueOf(color);
        mTextView.setTextColor(color);
    }

    /**
     * set icon
     *
     * @param mIcon 图片
     */
    public void setIcon(int mIcon) {
        this.mIcon = mIcon;
        mImageView.setImageResource(mIcon);
    }
}

package com.nurmemet.readbook.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.nurmemet.readbook.buidler.NurReaderThemeData;
import com.nurmemet.readbook.buidler.ReaderBuilder;
import com.nurmemet.readbook.myInreface.OnChangeListener;
import com.nurmemet.readbook.utils.AnimationUtil;

/**
 * Date:2020-11-09
 * Author:Nurmemet
 * Email:nur01@qq.com
 * Blog:https://www.cnblogs.com/nurmemet/
 */
public abstract class NurController implements OnChangeListener {

    public final LayoutInflater inflater;
    public final Context mContext;

    protected boolean isShowControl = false;
    public boolean isNightTheme = false;


    private int toolbarHeight;
    private int controllerHeight;
    private View toolbar;
    private View controller;
    private NurReaderThemeData mAttribute;
    private NurReaderView mNurReaderView;
    private ReaderBuilder mBuilder;

    public NurController(Context context) {
        mContext = context;
        inflater = LayoutInflater.from(context);
    }

    /**
     * toolbar
     */
    public abstract View getToolbar(ViewGroup group);

    /**
     * 控制器
     */
    public abstract View getController(ViewGroup group);


    /**
     * 把toolbar和controller的初始化
     */
    protected OnChangeListener init(NurReaderView nurReaderView, ReaderBuilder mBuilder) {
        if (nurReaderView == null) return null;
        mNurReaderView = nurReaderView;
        this.mBuilder = mBuilder;
        initLayout();
        return this;
    }


    /**
     * 关闭或打开
     */
    protected void showAndDismissControl() {
        mNurReaderView.post(() -> {
            controllerHeight = controller.getHeight();
            int toolbarEnd = 0;
            int toolbarStart = -toolbarHeight;
            int conStart = controllerHeight;
            int conEnd = 0;
            if (isShowControl) {
                toolbarEnd = toolbarStart;
                toolbarStart = 0;

                conEnd = conStart;
                conStart = 0;
            }
            AnimationUtil.startTranslationY(toolbar, toolbarStart, toolbarEnd);
            AnimationUtil.startTranslationY(controller, conStart, conEnd);
            isShowControl = !isShowControl;
        });
    }


    /**
     * 初始化view
     */
    private void initLayout() {
        controller = getController(mNurReaderView);
        toolbar = getToolbar(mNurReaderView);

        //toolbar
        setLayoutParams(RelativeLayout.ALIGN_PARENT_TOP, toolbar);
        mNurReaderView.addView(toolbar);

        //controller
        setLayoutParams(RelativeLayout.ALIGN_PARENT_BOTTOM, controller);
        mNurReaderView.addView(controller);


        //获取height
        mNurReaderView.post(() -> {
            controllerHeight = controller.getHeight();
            toolbarHeight = toolbar.getHeight();
            toolbar.setY(-toolbarHeight);
            controller.setY(-controllerHeight);
        });
        main();
    }

    /**
     * 初始化view后
     */
    public abstract void main();


    /**
     * 获取title
     */
    public String getTitle() {
        if (mBuilder != null) {
            return mBuilder.getTitle();
        }
        return "";
    }


    /**
     * 设置样式
     */
    public void setTheme(NurReaderThemeData attribute) {
        this.mAttribute = attribute;
        if (this.mAttribute == null || mNurReaderView == null) return;
        int bgColor = this.mAttribute.getBgColor();
        int controllerColor = mAttribute.getControllerColor();
        if (controllerColor != 0) {
            toolbar.setBackgroundColor(controllerColor);
            controller.setBackgroundColor(controllerColor);
        }
        if (bgColor != 0) {
            mNurReaderView.setBackgroundColor(bgColor);
        }
        mNurReaderView.setTheme(mAttribute);
    }


    /**
     * 自动翻页
     * 根据百分之
     */
    public void setCurrentPercent(int percent, int max) {
        if (mNurReaderView != null) {
            mNurReaderView.setCurrentPercent(percent, max);
        }
    }


    /**
     * set layout parmas
     */
    private void setLayoutParams(int verb, View view) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(verb);
        view.setLayoutParams(params);
    }

}

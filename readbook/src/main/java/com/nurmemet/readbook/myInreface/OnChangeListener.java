package com.nurmemet.readbook.myInreface;

public interface OnChangeListener {

    void onChange(int currentIndex, int maxIndex, float percent);

}

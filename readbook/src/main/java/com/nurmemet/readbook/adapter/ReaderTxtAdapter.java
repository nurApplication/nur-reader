package com.nurmemet.readbook.adapter;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nurmemet.readbook.R;
import com.nurmemet.readbook.buidler.NurReaderThemeData;
import com.nurmemet.readbook.utils.OnTouchReader;

import java.util.List;

/**
 * Date:2020-11-07
 * Author:Nurmemet
 * Email:nur01@qq.com
 * Blog:https://www.cnblogs.com/nurmemet/
 */
public class ReaderTxtAdapter extends RecyclerView.Adapter<VH> {

    private OnTouchReader.RenderTouchListener touchListener;
    private List<String> testList;
    private NurReaderThemeData textBuilder;


    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.nur_reader_item_text_view, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        String s = testList.get(position);
        holder.itemView.setOnTouchListener(new OnTouchReader(touchListener));

        ///init text view
        if (textBuilder != null) {
            float textSize = textBuilder.getTextSize();
            if (textSize != 0) {
                holder.textView.setTextSize(textSize);
            }
            int textColor = textBuilder.getTextColor();
            if (textColor != 0) {
                holder.textView.setTextColor(textColor);
            }

            Typeface typeface = textBuilder.getTypeface();
            if (typeface != null) {
                holder.textView.setTypeface(typeface);
            }
        }
        holder.textView.setText(s);
    }


    @Override
    public int getItemCount() {
        return testList == null ? 0 : testList.size();
    }


    public void setTouchListener(OnTouchReader.RenderTouchListener touchListener) {
        this.touchListener = touchListener;
    }

    public void setData(List<String> testList, NurReaderThemeData textBuilder) {
        this.testList = testList;
        this.textBuilder = textBuilder;
        notifyDataSetChanged();
    }

    public List<String> getData() {
        return testList;
    }
}

class VH extends RecyclerView.ViewHolder {

    TextView textView;

    public VH(@NonNull View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.nur_reader_item_text_view);
    }
}


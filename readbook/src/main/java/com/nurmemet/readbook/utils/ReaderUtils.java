package com.nurmemet.readbook.utils;

import android.text.Layout;
import android.text.TextPaint;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ReaderUtils {
    private static final String TAG = "ReaderUtilsNew";
    private final char kashida = 1600;

    public void start(TextView textView, String txt, OnResult result) {
        textView.setText(txt);
        textView.post(() -> {
            int width = textView.getWidth() - textView.getPaddingLeft() - textView.getPaddingRight();
            String resultTxt = getResultTxt(textView.getLayout(), width);
            textView.setText(resultTxt);
            sortStr(textView, resultTxt, result);
        });
    }


    private void sortStr(TextView textView, String txt, OnResult result) {
        List<String> stringList = new ArrayList<>();
        textView.post(() -> {
            Layout layout = textView.getLayout();
            int height = textView.getHeight();
            int lineHeight = textView.getLineHeight();
            int lineCount = layout.getLineCount();

            float _plCount = (float) height / lineHeight;
            int plCount = (int) _plCount - 2;//每页剪一行

            Log.e(TAG, "sortStr: " + _plCount);
            Log.e(TAG, "sortStr: " + plCount);

            float _pageSize = (float) lineCount / plCount;
            int pageSize = (int) _pageSize;
            if (_pageSize > pageSize) {
                pageSize += 1;
            }

            //计算前面每页剪一行的行数
            float v = (float) (pageSize * 2) / plCount;
            pageSize += (int) v;
            if (v > (int) v) {
                pageSize += 1;
            }

            int beginIndex = 0;
            boolean end = false;
            for (int i = 1; i < pageSize && !end; i++) {
                int _index = ((plCount - 1) * i) + (i - 1);
                if (_index >= lineCount) {
                    _index = lineCount - 1;
                    end = true;
                }
                int lineEnd = layout.getLineEnd(_index);
                stringList.add(txt.substring(beginIndex, lineEnd));
                beginIndex = lineEnd;
            }

            if (result != null) {
                result.result(stringList);
            }

        });
    }


    private String getResultTxt(Layout layout, int viewWidth) {
        int lineCount = layout.getLineCount();
        StringBuilder bf = new StringBuilder();
        TextPaint textPaint = layout.getPaint();
        String kashidaStr = kashida + "";
        String txt = layout.getText().toString();
        float kashW = textPaint.measureText(kashidaStr);
        int oldLineEnd = 0;
        for (int i = 0; i < lineCount; i++) {
            int lineEnd = layout.getLineEnd(i);
            String line = txt.substring(oldLineEnd, lineEnd);
            float lineTxtW = textPaint.measureText(line);
            int kashNum = (int) ((viewWidth - lineTxtW) / kashW);
            if (!line.contains("\n")) {
                int n = 0;
                for (int k = 0; k < kashNum; k++) {
                    while (n < line.length()) {
                        if (((line.charAt(n) == 'ى' || line.charAt(n) == 'ې')
                                && line.charAt(n + 1) != ' '
                                && line.charAt(n + 1) != '.'
                                && line.charAt(n + 1) != '،'
                                && line.charAt(n + 1) != '('
                                && line.charAt(n + 1) != '«'
                                && line.charAt(n + 1) != ')'
                                && line.charAt(n + 1) != '»'
                                && line.charAt(n + 1) != '-'
                                && line.charAt(n + 1) != '!'
                                && line.charAt(n + 1) != '؟' && line.charAt(n + 1) != ':')
                                || ((line.charAt(n) == 'ن' || line.charAt(n) == 'ت'
                                || line.charAt(n) == 'س'
                                || line.charAt(n) == 'ش'
                                || line.charAt(n) == 'ب'
                                || line.charAt(n) == 'پ'
                                || line.charAt(n) == 'ي'
                                || line.charAt(n) == 'م'
                                || line.charAt(n) == 'ج'
                                || line.charAt(n) == 'چ'
                                || line.charAt(n) == 'خ'
                                || line.charAt(n) == 'غ'
                                || line.charAt(n) == 'ق'
                                || line.charAt(n) == 'ف'
                                || line.charAt(n) == 'ھ'
                                || line.charAt(n) == 'ل'
                                || line.charAt(n) == 'ك'
                                || line.charAt(n) == 'گ' || line.charAt(n) == 'ڭ') && (line
                                .charAt(n + 1) == 'ن'
                                || line.charAt(n + 1) == 'ت'
                                || line.charAt(n + 1) == 'ر'
                                || line.charAt(n + 1) == 'ز'
                                || line.charAt(n + 1) == 'س'
                                || line.charAt(n + 1) == 'ش'
                                || line.charAt(n + 1) == 'ب'
                                || line.charAt(n + 1) == 'پ'
                                || line.charAt(n + 1) == 'ي'
                                || line.charAt(n + 1) == 'ج'
                                || line.charAt(n + 1) == 'چ'
                                || line.charAt(n + 1) == 'خ'
                                || line.charAt(n + 1) == 'غ'
                                || line.charAt(n + 1) == 'ق'
                                || line.charAt(n + 1) == 'ف'
                                || line.charAt(n + 1) == 'د'
                                || line.charAt(n + 1) == 'ھ'
                                || line.charAt(n + 1) == 'ۋ'
                                || line.charAt(n + 1) == 'ژ'
                                || line.charAt(n + 1) == 'ل'
                                || line.charAt(n + 1) == 'ك'
                                || line.charAt(n + 1) == 'گ'
                                || line.charAt(n + 1) == 'ڭ' || line
                                .charAt(n + 1) == 1600))
                                || ((line.charAt(n) == 'ئ' || line.charAt(n) == 'ن'
                                || line.charAt(n) == 'ت'
                                || line.charAt(n) == 'س'
                                || line.charAt(n) == 'ش'
                                || line.charAt(n) == 'ب'
                                || line.charAt(n) == 'پ'
                                || line.charAt(n) == 'ي'
                                || line.charAt(n) == 'م'
                                || line.charAt(n) == 'ج'
                                || line.charAt(n) == 'چ'
                                || line.charAt(n) == 'خ'
                                || line.charAt(n) == 'غ'
                                || line.charAt(n) == 'ق'
                                || line.charAt(n) == 'ف'
                                || line.charAt(n) == 'ھ'
                                || line.charAt(n) == 'ك'
                                || line.charAt(n) == 'گ' || line.charAt(n) == 'ڭ') && (line
                                .charAt(n + 1) == 'ا'
                                || line.charAt(n + 1) == 'ە' || line
                                .charAt(n + 1) == 1600))) {
                            line = line.substring(0, n + 1) + kashidaStr
                                    + line.substring(n + 1);
                            n++;
                            break;
                        }
                        n++;
                        if (n == line.length() - 1) {
                            if (k == 0) {
                                break;
                            } else {
                                n = 0;
                            }
                        }
                    }
                }
            }
            bf.append(line);
            oldLineEnd = lineEnd;
        }

        return bf.toString();
    }


    public interface OnResult {
        void result(List<String> sortData);
    }
}

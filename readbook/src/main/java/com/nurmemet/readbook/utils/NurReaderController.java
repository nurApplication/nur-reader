package com.nurmemet.readbook.utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.nurmemet.readbook.R;
import com.nurmemet.readbook.buidler.NurReaderThemeData;
import com.nurmemet.readbook.widget.FontSizeView;
import com.nurmemet.readbook.widget.NurController;
import com.nurmemet.readbook.widget.TabIconView;

import java.util.ArrayList;
import java.util.List;

/**
 * Date:2020-11-09
 * Author:Nurmemet
 * Email:nur01@qq.com
 * Blog:https://www.cnblogs.com/nurmemet/
 */
public class NurReaderController extends NurController implements View.OnClickListener, SeekBar.OnSeekBarChangeListener, FontSizeView.OnChangeCallbackListener {

    private final static String THEME_KEY = "isNightTheme_key";
    private final static String FONT_SIZE_INDEX_KEY = "font_size_index_key";
    public final static int TAB_BACK_VIEW = 4;
    public final static int TAB_COLLECTION_VIEW = 5;
    private final OnClickIconListener iconListener;
    private View mToolbar;
    private View mController;
    private TabIconView mTabNight, mTabTextSize;
    private NurReaderThemeData attribute;
    private ImageView backBtn, collBtn;
    private FontSizeView mFontSizeView;
    private SeekBar mSeekBar;
    private boolean isShowSizeControl = false;
    private View mSeekBarBox;

    private List<Integer> fontSizes;
    private TextView mTitleTv;
    private int mTextSizeIndex = 1;

    public NurReaderController(Context context, OnClickIconListener iconListener) {
        super(context);
        this.iconListener = iconListener;
        fontSizes = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            fontSizes.add(12 + (4 * i));
        }

    }

    @Override
    public void main() {
        isNightTheme = (boolean) SPUtils.getParam(mContext, THEME_KEY, false);
        mTextSizeIndex = (int) SPUtils.getParam(mContext, FONT_SIZE_INDEX_KEY, mTextSizeIndex);
        updateNightTheme();
    }

    @Override
    public View getToolbar(ViewGroup group) {
        mToolbar = inflater.inflate(R.layout.nur_reader_controller_toolbar, group, false);
        backBtn = mToolbar.findViewById(R.id.nur_reader_toolbarBackBtn);
        backBtn.setTag(TAB_BACK_VIEW);
        backBtn.setOnClickListener(this);
        collBtn = mToolbar.findViewById(R.id.nur_reader_toolbarCollBtn);
        collBtn.setTag(TAB_COLLECTION_VIEW);
        collBtn.setOnClickListener(this);
        return mToolbar;
    }

    @Override
    public View getController(ViewGroup group) {
        mController = inflater.inflate(R.layout.nur_reader_controller_layout, group, false);
        mTitleTv = mController.findViewById(R.id.nur_reader_controller_title_view);

        mTitleTv.setText(getTitle());

        //seekBar
        mSeekBar = mController.findViewById(R.id.nur_reader_controller_seek_bar);
        mSeekBar.setOnSeekBarChangeListener(this);


        mSeekBarBox = mController.findViewById(R.id.nur_reader_controller_seekBox);


        mFontSizeView = mController.findViewById(R.id.nur_reader_controller_font_size_view);
        mFontSizeView.setChangeCallbackListener(this);

        mTabTextSize = mController.findViewById(R.id.nur_reader_tab_text_size);
        mTabNight = mController.findViewById(R.id.nur_reader_tab_night);

        mTabTextSize.setOnClickListener(this);
        mTabNight.setOnClickListener(this);
        return mController;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.nur_reader_toolbarBackBtn) {
            iconListener.onBackPress(v);
        } else if (id == R.id.nur_reader_toolbarCollBtn) {
            if (iconListener != null) {
                iconListener.onCollectionPress(v);
            }
        } else if (id == R.id.nur_reader_tab_text_size) {
            updateFontSize();
        } else if (id == R.id.nur_reader_tab_night) {
            updateNightTheme();
        }
    }


    /**
     * 更改字体大小
     */
    private void updateFontSize() {
        if (isShowSizeControl) {
            mSeekBarBox.setVisibility(View.VISIBLE);
            mFontSizeView.setVisibility(View.GONE);
        } else {
            mSeekBarBox.setVisibility(View.GONE);
            mFontSizeView.setVisibility(View.VISIBLE);
        }
        isShowSizeControl = !isShowSizeControl;
    }


    /**
     * 更改夜间模式
     */
    private void updateNightTheme() {
        if (attribute == null)
            attribute = new NurReaderThemeData();

        if (!isNightTheme) {
            attribute.setTextColor(ResourcesUtils.getColor(mContext, R.color.gray2));
            attribute.setBgColor(ResourcesUtils.getColor(mContext, R.color.book_bg_night));
            attribute.setControllerColor(ResourcesUtils.getColor(mContext, R.color.book_control_night));
        } else {
            attribute.setTextColor(ResourcesUtils.getColor(mContext, R.color.text_color));
            attribute.setBgColor(ResourcesUtils.getColor(mContext, R.color.book_bg));
            attribute.setControllerColor(ResourcesUtils.getColor(mContext, R.color.white));
        }
        int color = attribute.getTextColor();
        mTabTextSize.setTextColor(color);
        mTabNight.setTextColor(color);
        mTitleTv.setTextColor(color);
        attribute.setTextSize(fontSizes.get(mTextSizeIndex));
        mFontSizeView.setDefaultPosition(mTextSizeIndex);
        ColorStateList tint = ColorStateList.valueOf(color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            collBtn.setImageTintList(tint);
            backBtn.setImageTintList(tint);
        }

        SPUtils.putParam(mContext, THEME_KEY, isNightTheme);
        isNightTheme = !isNightTheme;
        setTheme(attribute);
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        setCurrentPercent(seekBar.getProgress(), seekBar.getMax());
    }

    @Override
    public void onChangeListener(int position) {
        if (attribute == null)
            attribute = new NurReaderThemeData();

        if (position > -1 && fontSizes.size() - 1 > position) {
            mTextSizeIndex = position;
            attribute.setTextSize(fontSizes.get(position));
            setTheme(attribute);
            SPUtils.putParam(mContext, FONT_SIZE_INDEX_KEY, mTextSizeIndex);
        }
    }

    @Override
    public void onChange(int currentIndex, int maxIndex, float percent) {
        mSeekBar.setProgress((int) (currentIndex * ((float) mSeekBar.getMax() / maxIndex)));
    }


    public interface OnClickIconListener {

        /**
         * 返回按钮点击
         */
        void onBackPress(View v);


        /**
         * 点击收藏按钮
         */
        void onCollectionPress(View v);
    }

}

package com.nurmemet.readbook.utils;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Date:2020-11-07
 * Author:Nurmemet
 * Email:nur01@qq.com
 * Blog:https://www.cnblogs.com/nurmemet/
 */
public class OnTouchReader implements View.OnTouchListener {
    private static final String TAG = "OnTouchBookController";


    private final RenderTouchListener bookClickListener;
    float downX = 0;
    float downY = 0;

    public OnTouchReader(RenderTouchListener listener) {
        bookClickListener = listener;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (bookClickListener == null) return false;
        int action = event.getAction();
        int width = v.getWidth();
        if (action == MotionEvent.ACTION_DOWN) {
            downX = event.getX();
            downY = event.getY();
        } else if (action == MotionEvent.ACTION_UP) {
            int cX = width / 3;
            if (downX < cX) {
                Log.e(TAG, "onTouch: clickLeft");
                bookClickListener.onClickLeft(v);
            } else if (downX > cX && downX < cX * 2) {
                Log.e(TAG, "onTouch: clickCenter");
                bookClickListener.onClickCenter(v);
            } else {
                Log.e(TAG, "onTouch: clickRight");
                bookClickListener.onClickRight(v);
            }
        }
        return true;
    }

    public interface RenderTouchListener {
        void onClickLeft(View view);

        void onClickCenter(View view);

        void onClickRight(View view);
    }
}

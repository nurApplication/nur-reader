package com.nurmemet.readbook.utils;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class FileUtils {

    public static String PATH = Environment.getExternalStorageDirectory() + "/";

    public static String readTxt(String path) {
        StringBuilder str = new StringBuilder();
        try {
            File urlFile = new File(path);
            InputStreamReader isr = new InputStreamReader(new FileInputStream(urlFile), "UTF-8");
            BufferedReader br = new BufferedReader(isr);

            String mimeTypeLine = null;
            while ((mimeTypeLine = br.readLine()) != null) {
                str.append("    ").append(mimeTypeLine).append("\n");
            }
            isr.close();
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("TAG", "readTxt: " + e.getMessage());
        }
        return str.toString();
    }
}

package com.nurmemet.readbook.utils;

import android.animation.ObjectAnimator;
import android.view.View;

/**
 * Date:2020-11-07
 * Author:Nurmemet
 * Email:nur01@qq.com
 * Blog:https://www.cnblogs.com/nurmemet/
 */
public class AnimationUtil {

    /**
     * 开始动画
     */
    public static void startTranslationY(View view, float... values) {
        startTranslationY(350, view, values);
    }

    /**
     * 开始动画
     */
    public static void startTranslationY(long duration, View view, float... values) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "translationY", values);
        animator.setDuration(duration);
        animator.start();
    }

}

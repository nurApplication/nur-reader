package com.nurmemet.readbook.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.nurmemet.readbook.widget.BatteryView;


public class BatteryBroadcastReciver extends BroadcastReceiver {

    private final BatteryView view;

    public BatteryBroadcastReciver(BatteryView view) {
        this.view = view;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (view == null) return;
        String action = intent.getAction();
        //如果捕捉到的Action是ACTION_BATTERY_CHANGED则运行onBatteryInforECEIVER()
        if (Intent.ACTION_BATTERY_CHANGED.equals(action)) {
            //获得当前电量
            int intLevel = intent.getIntExtra("level", 0);
            //获得手机总电量
            int intScale = intent.getIntExtra("scale", 100);
            // 在下面会定义这个函数，显示手机当前电量
            int percent = intLevel * 100 / intScale;
            view.setPower(percent);
        }
    }

}

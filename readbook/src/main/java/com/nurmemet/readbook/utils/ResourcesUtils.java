package com.nurmemet.readbook.utils;

import android.content.Context;

public class ResourcesUtils {


    public static int getColor(Context context, int colorId) {
        return context.getResources().getColor(colorId);
    }

}

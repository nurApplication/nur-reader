package com.nurmemet.readbook.buidler;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.nurmemet.readbook.myInreface.OnChangeListener;
import com.nurmemet.readbook.widget.NurController;

/**
 * Date:2020-11-07
 * Author:Nurmemet
 * Email:nur01@qq.com
 * Blog:https://www.cnblogs.com/nurmemet/
 */
public abstract class ReaderBuilder {

    private LinearLayoutManager layoutManager;
    private boolean isStackFromEnd;
    private OnChangeListener onChangeListener;
    private NurController nurController;

    public ReaderBuilder(Context context) {
        layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
    }


    /**
     * recycler view layout manager
     * 默认情况下HORIZONTAL的LinearLayoutManager
     *
     * @param layoutManager readerRecycler.setLayoutManager
     */
    public ReaderBuilder setLayoutManager(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
        return this;
    }


    /**
     * get layout manager
     */
    public LinearLayoutManager getLayoutManager() {
        return layoutManager;
    }

    /**
     * 布局反向
     *
     * @param value true 布局反向
     */
    public ReaderBuilder setStackFromEnd(boolean value) {
        isStackFromEnd = value;
        layoutManager.setReverseLayout(value);//布局反向
        layoutManager.setStackFromEnd(value);//数据反向
        return this;
    }

    /**
     * 是否布局反向
     */
    public boolean isStackFromEnd() {
        return isStackFromEnd;
    }


    /**
     * 监听翻页。。。
     */
    public ReaderBuilder addOnChangeListener(OnChangeListener onChangeListener) {
        this.onChangeListener = onChangeListener;
        return this;
    }


    /**
     * OnChangeListener
     */
    public OnChangeListener getChangeListener() {
        return onChangeListener;
    }


    /**
     * 控制器
     */
    public ReaderBuilder setController(NurController nurController) {
        this.nurController = nurController;
        return this;
    }

    /**
     * 控制器
     */
    public NurController getNurController() {
        return nurController;
    }

    /**
     * 数据
     */
    public abstract String getData();


    /**
     * 题目
     */
    public abstract String getTitle();

}

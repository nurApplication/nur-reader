package com.nurmemet.readbook.buidler;

import android.graphics.Typeface;

public class NurReaderThemeData {

    private float textSize;
    private int textColor;
    private int bgColor;
    private int controllerColor;

    public int getBgColor() {
        return bgColor;
    }

    public void setBgColor(int bgColor) {
        this.bgColor = bgColor;
    }

    public int getControllerColor() {
        return controllerColor;
    }

    public void setControllerColor(int controllerColor) {
        this.controllerColor = controllerColor;
    }

    public Typeface getTypeface() {
        return typeface;
    }

    public void setTypeface(Typeface typeface) {
        this.typeface = typeface;
    }

    private Typeface typeface;

    public float getTextSize() {
        return textSize;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
    }


    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }
}

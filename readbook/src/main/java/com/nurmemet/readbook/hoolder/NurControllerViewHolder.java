package com.nurmemet.readbook.hoolder;

import android.view.View;
import android.widget.TextView;

/**
 * Date:2020-11-09
 * Author:Nurmemet
 * Email:nur01@qq.com
 * Blog:https://www.cnblogs.com/nurmemet/
 */
public interface NurControllerViewHolder {

    TextView getTitleView();

    View getSettingsTabView();

    View getNightThemeTabView();

    View getTextSizeTabView();

}
